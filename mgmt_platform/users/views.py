from django.shortcuts import redirect, render
from django.conf import settings
from django.contrib.auth import authenticate, login

# Create your views here.
def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('%s?next=%s' % (settings.HOMEPAGE, request.path))

        else:
            return render(request, 'users/templates/login.html', {'error': 'Login unsuccessful, please try again.'})

    if request.method == 'GET':
        return render(request, 'users/templates/login.html')