from django.db import models

# Create your models here.
class Folder(models.Model):
    name = models.CharField(max_length=30)
    parent = models.ForeignKey("Folder")