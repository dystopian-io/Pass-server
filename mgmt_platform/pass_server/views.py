from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

#@login_required
def index(request):
    if request.method == 'GET':
        return render(request, 'pass_server/templates/index.html')


def treeview(request):
    if request.method == 'GET':
        return JsonResponse({
            'response':'success',
            'data': [
                {
                    'id': 1,
                    'parent': 'None',
                    'name': 'Apple',
                },
                {
                    'id': 2,
                    'parent': 'None',
                    'name': 'Banking',
                },
                {
                    'id': 3,
                    'parent': 'None',
                    'name': 'Dystopian',
                },
                {
                    'id': 4,
                    'parent': 1,
                    'name': 'Iphone',
                },
                {
                    'id': 5,
                    'parent': 1,
                    'name': 'Ipad',
                },
                {
                    'id': 6,
                    'parent': 3,
                    'name': 'Github',
                },
                {
                    'id': 7,
                    'parent': 5,
                    'name': 'IOS',
                },
                {
                    'id': 8,
                    'parent': 6,
                    'name': 'VCS',
                }
            ],
        })