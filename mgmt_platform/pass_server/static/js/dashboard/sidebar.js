
    function buildTreeView(ajax_response) {

        $.each(ajax_response.data, function(key, value) {

            if(value["parent"] == 'None') {
                $(".treeview-menu").append("<li id=\"folder" + value["id"] + "\"><a href=\"#\"><i class=\"fa fa-folder\"></i>" + value["name"] + "</a></li>");
            } else {
                var parentID = "#folder"+value["parent"];
                if($(parentID).find("ul.treeview-menu").length == 0)
                {
                    $(parentID).addClass("treeview");
                    $(parentID + " a").append("<span class=\"pull-right-container\">\n" +
                                                    "<i class=\"fa fa-angle-left pull-right\"></i>\n" +
                                             "</span>");
                    $(parentID).append("<ul class=\"treeview-menu\">");
                }
                $(parentID + " ul").append("<li id=\"folder"+ value["id"] +"\"><a href=\"#\"><i class=\"fa fa-folder\"></i>"+ value["name"] +"</a></li>");
            }
        });
    }

    function getTreeView(ajax_csrf) {

        var ajax_url = "treeview";
        var ajax_data = "";
        var ajax_type = "GET";

        ajax_request(ajax_url, ajax_data, ajax_type, ajax_csrf, buildTreeView);

    }


