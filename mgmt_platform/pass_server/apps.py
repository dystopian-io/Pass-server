from django.apps import AppConfig


class PassServerConfig(AppConfig):
    name = 'pass_server'
