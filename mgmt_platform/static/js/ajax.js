        // Ajax request
      // =================
      function ajax_request(url, data, type, csrf, successFunction) {
        $.ajaxSetup({
            data: {csrfmiddlewaretoken: csrf },
        });

        $.ajax({
            url: url,
            data: data,
            dataType: 'json',
            success: successFunction
          });
      }
